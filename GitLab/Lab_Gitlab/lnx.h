/*! \file
\brief Заголовочный файл функции для вычисления натурального логарифма от x
*/
#ifndef LNX_H
#define LNX_H
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение натурального логарифма от x с точностью e
*/
long double lnX(long double x, long double e);

#endif // LNX_H
