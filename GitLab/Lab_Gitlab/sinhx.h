/*! \file
\brief Заголовочный файл функции для вычисления гиперболического синуса от x
*/
#ifndef SINHX_H
#define SINHX_H
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение гиперболического косинуса от x с точностью e
*/
long double sinHX(long double x, long double e);

#endif // SINHX_H
