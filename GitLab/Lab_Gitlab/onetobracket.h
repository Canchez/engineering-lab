/*! \file
\brief Заголовочный файл функции для вычисления 1/(1+x)
*/
#ifndef ONETOBRACKET_H
#define ONETOBRACKET_H
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение 1/(1+x) с точностью e
*/
long double oneToBracketX(long double x, long double e);

#endif // ONETOBRACKET_H
