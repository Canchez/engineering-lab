#include <QtTest>
#include "tst_tst_tests_gitlab.h"
#include "../einx.h"
#include "../sinx.h"
#include "../cosx.h"
#include "../lnx.h"
#include "../bracketina.h"
#include "../arctgx.h"
#include "../onetobracket.h"
#include "../sinhx.h"


void tst_tests_gitlab::test_eInX()
{
    QCOMPARE(eInX(1, 1), 2);
    QVERIFY(qFuzzyCompare(2.5+1.0/6, eInX(1, 0.05)));
    QVERIFY(qFuzzyCompare(12.181743191243, eInX(2.5, 0.002)));
}

void tst_tests_gitlab::test_sinX()
{
    QCOMPARE(sinX(0,0.1), 0);
    QVERIFY(qFuzzyCompare(1-1.0/6+1.0/120, sinX(1, 0.008)));
    QVERIFY(qFuzzyCompare(0.5990461991998, sinX(2.5, 0.002)));
}

void tst_tests_gitlab::test_cosX()
{
    QCOMPARE(cosX(0, 0.1), 1);
    QVERIFY(qFuzzyCompare(0.5+1.0/24, cosX(1, 0.008)));
    QVERIFY(qFuzzyCompare(-0.8012638865745, cosX(2.5, 0.002)));
}

void tst_tests_gitlab::test_lnX()
{
    QCOMPARE(lnX(0, 0.1), 0);
    QVERIFY(qFuzzyCompare(0.359375+0.125/3, lnX(0.5, 0.008)));
    QVERIFY(qFuzzyCompare(-0.6911458333333, lnX(-0.5, 0.002)));
}

void tst_tests_gitlab::test_bracket()
{
    QCOMPARE(bracketInA(0, 0.1, 1), 1);
    QVERIFY(qFuzzyCompare(3.656, bracketInA(0.5, 0.008, 3.2)));
    QVERIFY(qFuzzyCompare(0.25, bracketInA(-0.5, 0.002, 2)));
}

void tst_tests_gitlab::test_arctgX()
{
    QCOMPARE(arctgX(0, 0.1), 0);
    QVERIFY(qFuzzyCompare(0.5-0.125/3, arctgX(0.5, 0.008)));
    QVERIFY(qFuzzyCompare(-0.32+0.32*0.32*0.32/3, arctgX(-0.32, 0.002)));
}

void tst_tests_gitlab::test_oneToX()
{
    QCOMPARE(oneToBracketX(0, 0.1), 1);
    QVERIFY(qFuzzyCompare(1.984375, oneToBracketX(0.5, 0.008)));
    QVERIFY(qFuzzyCompare(0.7567623168, oneToBracketX(-0.32, 0.002)));
}

void tst_tests_gitlab::test_sinHX()
{
    QCOMPARE(sinHX(0, 0.1), 0);
    QVERIFY(qFuzzyCompare(0.5+0.125/6, sinHX(0.5, 0.008)));
    QVERIFY(qFuzzyCompare(-0.32+(-0.032768/6), sinHX(-0.32, 0.002)));
}
