#ifndef TST_TST_TESTS_GITLAB_H
#define TST_TST_TESTS_GITLAB_H

#include <QtTest>

class tst_tests_gitlab : public QObject
{
    Q_OBJECT
public:

private slots:
    void test_eInX();
    void test_sinX();
    void test_cosX();
    void test_lnX();
    void test_bracket();
    void test_arctgX();
    void test_oneToX();
    void test_sinHX();

};

#endif // TST_TST_TESTS_GITLAB_H
