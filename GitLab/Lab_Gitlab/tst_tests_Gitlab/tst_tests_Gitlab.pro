QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_tst_tests_gitlab.cpp \
        ../arctgx.cpp \
        ../bracketina.cpp \
        ../cosx.cpp \
        ../einx.cpp \
        ../lnx.cpp \
        ../sinx.cpp \
        ../onetobracket.cpp \
        ../sinhx.cpp \
        main.cpp

HEADERS += \
        ../arctgx.h \
        ../bracketina.h \
        ../cosx.h \
        ../einx.h \
        ../lnx.h \
        ../sinx.h \
        ../onetobracket.h \
        ../sinhx.h \
    tst_tst_tests_gitlab.h
