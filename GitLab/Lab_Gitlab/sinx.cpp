/*! \file
\brief Файл исходного кода функции для вычисления синуса от x
*/
#include "sinx.h"
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение косинуса от x с точностью e
*/
long double sinX(long double x, long double e)
{
    long double curUpper = x;
    long long int curBottom = 1;
    long long int n = 1;
    int sign = -1;
    long double add = 0;
    long double res = x;
    do
    {
        curUpper = curUpper * x * x;
        curBottom = curBottom * (n+1) * (n+2);
        add = curUpper / curBottom * sign;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        n = n+2;
        sign = -sign;
    } while ((add>=e) || (add<=-e));
    return res;
}
