/*! \file
\brief Файл исходного кода функции для вычисления (1+x) в степени a
*/
#include "bracketina.h"
/*!
\param[in] x Аргумент
\param[in] e Точность
\param[in] a Степень скобки
\return Значение (1+x) в степени a с точностью e
*/
long double bracketInA(long double x, long double e, long double a)
{
    long double curUpper = 1;
    long long int curBottom = 1;
    long double curA = a;
    long double add = 0;
    long long int n = 1;
    long double res = 1;
    do
    {
        curUpper = curUpper * x;
        curBottom = curBottom * n;
        add = curA * curUpper / curBottom ;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        curA = curA * (a - n);
        n++;
    } while ((add>=e) || (add<=-e));
    return res;
}
