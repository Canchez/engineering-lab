/*! \file
\brief Заголовочный файл класса, реализующего функционал проекта
\details Файл, в котором объявлены все объекты, виджеты, слоты и функции проекта
*/
#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDir>
#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QString>
#include <QCoreApplication>
#include <QDebug>
#include <sstream>

/*! \brief Класс, реализующий функционал проекта
\details Класс, в котором создаются все объекты, виджеты, слоты, функции проекта.
*/
class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);

    QPushButton* pbEInX; //!< Кнопка для числа Эйлера в степени x
    QPushButton* pbSinX; //!< Кнопка для синуса от x
    QPushButton* pbCosX; //!< Кнопка для косинуса от x
    QPushButton* pbLnX; //!< Кнопка для натурального логарифма от x
    QPushButton* pbBracketX; //!< Кнопка для (1+x) в степени a
    QPushButton* pbArctgX; //!< Кнопка для арктангенса от x
    QPushButton* pbOneToBracketX; //!< Кнопка 1/(1+x)
    QPushButton* pbSinHX; //!< Кнопка для гиперболического синуса от x

    QPushButton* pbCalculate; //!< Кнопка для выполнения расчётов

    QVBoxLayout* funcsLayout; //!< Вертикальная компоновка для кнопок функций
    QVBoxLayout* leftLayout; //!< Вертикальная компоновка для меню слева
    QGridLayout* labelLayout; //!< Компоновка-таблица для виджетов ввода значений
    QHBoxLayout* answerLayout; //!< Горизонтальная компоновка для ответа
    QVBoxLayout* answLabelLayout; //!< Горизонтальная компоновка для блока с ответом
    QHBoxLayout* mainLayout; //!< Горизонтальная компоновка для всего проекта

    QLabel* lbChosen; //!< Метка с текстом "Выбранная функция:"
    QLabel* lbFunc; //!< Метка для выбранной функции

    QLabel* lbLineForX; //!< Метка для ввода x
    QLabel* lbLineForE; //!< Метка для ввода e
    QLabel* lbLineForA; //!< Метка для ввода a

    QDoubleSpinBox* sbForX; //!< DoubleSpinBox для ввода x
    QDoubleSpinBox* sbForE; //!< DoubleSpinBox для ввода e
    QDoubleSpinBox* sbForA; //!< DoubleSpinBox для ввода a

    QLabel* lbAnswer; //!< Метка для овтета
    QLineEdit* leForAnswer; //!< Поле вывод ответа

    QString path; //!< Путь к папке с картинками

    QString qStringFromLongDouble(const long double myLongDouble); //!< Функция для перевода long double в QString

    //! Нумератор для функций
    enum func
    {
        ex,
        sin,
        cos,
        ln,
        bracket,
        arctg,
        oneDiv,
        sinh
    };
public slots:
    void funcChosen(int num);
    void calculate();
};

#endif // WIDGET_H
