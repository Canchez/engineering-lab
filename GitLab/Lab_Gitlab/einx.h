/*! \file
\brief Заголовочный файл функции для вычисления числа Эйлера в степени x
*/
#ifndef EINX_H
#define EINX_H
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение числа Эйлера в степени x с точностью e
*/
long double eInX(long double x, long double e);

#endif // EINX_H
