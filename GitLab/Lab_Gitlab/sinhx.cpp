/*! \file
\brief Файл исходного кода функции для вычисления гиперболического синуса от x
*/
#include "sinhx.h"
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение гиперболического косинуса от x с точностью e
*/
long double sinHX(long double x, long double e)
{
    long double curUpper = x;
    long long int curBottom = 1;
    long long int n = 1;
    long double add = 0;
    long double res = x;
    do
    {
        curUpper = curUpper * x * x;
        curBottom = curBottom * (n+1) * (n+2);
        add = curUpper / curBottom;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        n = n+2;
    } while ((add>=e) || (add<=-e));
    return res;
}
