/*! \file
\brief Файл исходного кода для вычисления арктангенса от x
*/
#include "arctgx.h"
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение арктангенса от x с точностью e
*/
long double arctgX(long double x, long double e)
{
    long double curUpper = x;
    long long int curBottom = 1;
    int sign = -1;
    long double add = 0;
    long double res = x;
    do
    {
        curUpper = curUpper * x * x;
        curBottom = curBottom + 2;
        add = curUpper / curBottom * sign;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        sign = -sign;
    } while ((add>=e) || (add<=-e));
    return res;
}
