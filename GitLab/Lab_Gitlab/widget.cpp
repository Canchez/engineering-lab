/*! \file
\brief Файл исходного кода класса, реализующего функционал проекта
\details Файл, в котором реализованы слоты и функции проекта
*/
#include "widget.h"

#include "arctgx.h"
#include "bracketina.h"
#include "cosx.h"
#include "einx.h"
#include "lnx.h"
#include "sinx.h"
#include "onetobracket.h"
#include "sinhx.h"
/*!
\brief Конструктор класса
Создает объект класса Widget, в нем инициализирует все объекты, устанавливает нужные размеры и подключает сигналы и слоты.
*/
Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QDir dir(QApplication::applicationDirPath());
    dir.cdUp();
    dir.cd("pics");
    path = dir.path()+"/";
    pbCalculate = new QPushButton("Вычислить");
    pbCalculate->setFixedHeight(50);

    funcsLayout = new QVBoxLayout();
    leftLayout = new QVBoxLayout();
    labelLayout = new QGridLayout();
    answerLayout = new QHBoxLayout();
    answLabelLayout = new QVBoxLayout();
    mainLayout = new QHBoxLayout();

    lbChosen = new QLabel("Выбранная функция:");
    lbLineForX = new QLabel("Ввод x:");
    lbLineForE = new QLabel("Ввод точности:");
    lbLineForA = new QLabel("Ввод альфа:");
    lbFunc = new QLabel("Не выбрано");
    lbFunc->setFixedSize(QSize(226, 93));
    lbChosen->setAlignment(Qt::AlignHCenter);
    lbFunc->setAlignment(Qt::AlignHCenter);

    sbForX = new QDoubleSpinBox();
    sbForE = new QDoubleSpinBox();
    sbForA = new QDoubleSpinBox();
    leForAnswer = new QLineEdit();

    sbForE->setDecimals(3);
    sbForE->setRange(0.001, 100);
    sbForE->setSingleStep(0.001);
    sbForA->setRange(-100, 100);
    sbForA->setDecimals(3);
    sbForA->setSingleStep(0.001);
    leForAnswer->setReadOnly(true);
    leForAnswer->hide();

    lbAnswer = new QLabel("Ответ:");
    lbAnswer->hide();

    QSize buttonsSize(100, 40);
    QVector<QPushButton*> buttons;
    buttons.append(pbEInX);
    buttons.append(pbSinX);
    buttons.append(pbCosX);
    buttons.append(pbLnX);
    buttons.append(pbBracketX);
    buttons.append(pbArctgX);
    buttons.append(pbOneToBracketX);
    buttons.append(pbSinHX);

    for (int i = 0; i < buttons.size(); i++)
    {
        buttons[i] = new QPushButton();
        buttons[i]->setFixedSize(buttonsSize);
        buttons[i]->setIcon(QIcon(path+QString::number(i+1)+".png"));
        buttons[i]->setIconSize(buttonsSize);
        funcsLayout->addWidget(buttons[i]);
        connect(buttons[i], &QPushButton::clicked, [=] () { funcChosen(i); });
    }
    connect(pbCalculate, SIGNAL(clicked()), this, SLOT(calculate()));
    mainLayout->addLayout(funcsLayout);
    leftLayout->addWidget(lbChosen);
    leftLayout->addWidget(lbFunc);
    labelLayout->addWidget(lbLineForX);
    labelLayout->addWidget(lbLineForX, 0, 0);
    labelLayout->addWidget(lbLineForE, 0, 1);
    labelLayout->addWidget(lbLineForA, 0, 2);
    labelLayout->addWidget(sbForX, 1, 0);
    labelLayout->addWidget(sbForE, 1, 1);
    labelLayout->addWidget(sbForA, 1, 2);
    lbLineForX->hide();
    lbLineForE->hide();
    sbForX->hide();
    sbForE->hide();
    pbCalculate->hide();
    lbLineForA->hide();
    sbForA->hide();
    leftLayout->addLayout(labelLayout);
    answLabelLayout->addWidget(lbAnswer);
    answLabelLayout->addWidget(leForAnswer);
    answerLayout->addLayout(answLabelLayout);
    answerLayout->addWidget(pbCalculate);
    leftLayout->addLayout(answerLayout);
    mainLayout->addLayout(leftLayout);
    setLayout(mainLayout);
}

/*!
 * \brief Слот, вызывающийся при выборе функции
 * \param[in] num Номер выбранной функции
 * \details Слот, видоизменяющий окно проекта для вычисления выбранной функции
 */
void Widget::funcChosen(int num)
{
    if (lbLineForX->isHidden())
    {
        lbLineForX->show();
        lbLineForE->show();
        sbForX->show();
        sbForE->show();
        pbCalculate->show();
        lbAnswer->show();
        leForAnswer->show();
    }
    if (num == 4 && lbLineForA->isHidden())
    {
        lbLineForA->show();
        sbForA->show();
    }
    else if (num != 4 && !lbLineForA->isHidden())
    {
        lbLineForA->hide();
        sbForA->hide();
    }
    lbFunc->clear();
    lbFunc->setPixmap(QPixmap(path+QString::number(num+1)+".png"));
    lbChosen->setWhatsThis(QString::number(num));
    switch (num)
    {
    case ex:
    case sin:
    case cos:
    {
        sbForX->setRange(-1000, 1000);
        sbForX->setSingleStep(0.1);
        sbForX->setDecimals(3);
        break;
    }
    case ln:
    {
        sbForX->setRange(-0.9999, 1);
        sbForX->setSingleStep(0.0001);
        sbForX->setDecimals(4);
        break;
    }
    case bracket:
    {
        sbForX->setRange(-0.9999, 0.9999);
        sbForX->setSingleStep(0.0001);
        sbForX->setDecimals(4);
        break;
    }
    case arctg:
    {
        sbForX->setRange(-1, 1);
        sbForX->setSingleStep(0.0001);
        sbForX->setDecimals(4);
        break;
    }
    case oneDiv:
    {
        sbForX->setRange(-0.9999, 0.9999);
        sbForX->setSingleStep(0.0001);
        sbForX->setDecimals(4);
        break;
    }
    case sinh:
    {
        sbForX->setRange(-1000, 1000);
        sbForX->setSingleStep(0.1);
        sbForX->setDecimals(3);
        break;
    }
    }
}

/*!
\brief Функция для перевода long double в QString
\param myLongDouble Число
\return Строка, содержащая число
*/
QString Widget::qStringFromLongDouble(const long double myLongDouble)
{
  std::stringstream ss;
  ss << myLongDouble;

  return QString::fromStdString(ss.str());
}

/*!
 * \brief Слот, вызывающийся при нажатии на кнопку "Рассчитать"
 * \details Слот вызывает нужную функцию для расчёта и помещает вычисленное значение в QLineEdit, выделенный для ответа
 */
void Widget::calculate()
{
    int num = lbChosen->whatsThis().toInt();
    long double x = sbForX->value();
    long double e = sbForE->value();
    long double answer = 0;
    switch (num)
    {
    case ex:
        answer = eInX(x, e);
        break;
    case sin:
        answer = sinX(x, e);
        break;
    case cos:
        answer = cosX(x, e);
        break;
    case ln:
        answer = lnX(x, e);
        break;
    case bracket:
    {
        double a = sbForA->value();
        answer = bracketInA(x, e, a);
        break;
    }
    case arctg:
        answer = arctgX(x, e);
        break;
    case oneDiv:
        answer = oneToBracketX(x, e);
        break;
    case sinh:
        answer = sinHX(x, e);
        break;
    }
    leForAnswer->setText(qStringFromLongDouble(answer));
}
