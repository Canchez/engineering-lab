/*! \file
\brief Заголовочный файл функции для вычисления косинуса от x
*/
#ifndef COSX_H
#define COSX_H
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение косинуса от x с точностью e
*/
long double cosX(long double x, long double e);

#endif // COSX_H
