/*! \file
\brief Файл исходного кода функции для вычисления 1/(1+x)
*/
#include "onetobracket.h"
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение 1/(1+x) с точностью e
*/
long double oneToBracketX(long double x, long double e)
{
    long double curUpper = 1;
    long double add = 0;
    long double res = 1;
    do
    {
        curUpper = curUpper * x;
        add = curUpper;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
    } while ((add>=e) || (add<=-e));
    return res;
}
