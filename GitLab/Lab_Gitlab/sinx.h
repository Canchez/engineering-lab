/*! \file
\brief Заголовочный файл функции для вычисления синуса от x
*/
#ifndef SINX_H
#define SINX_H
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение косинуса от x с точностью e
*/
long double sinX(long double x, long double e);

#endif // SINX_H
