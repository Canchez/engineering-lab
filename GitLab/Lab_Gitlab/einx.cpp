/*! \file
\brief Файл исходного кода функции для вычисления числа Эйлера в степени x
*/
#include "einx.h"
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение числа Эйлера в степени x с точностью e
*/
long double eInX(long double x, long double e)
{
    long double curUpper = 1;
    long long int curBottom = 1;
    long long int n = 1;
    long double add = 0;
    long double res = 1;
    do
    {
        curUpper *= x;
        curBottom *= n;
        add = curUpper / curBottom;
        if ((add>=e) || (add <= -e))
        {
            res += add;
        }
        n++;
    } while ((add>=e) || (add <= -e));
    return res;
}
