/*! \file
\brief Заголовочный файл функции для вычисления арктангенса от x
*/
#ifndef ARCTGX_H
#define ARCTGX_H
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение арктангенса от x с точностью e
*/
long double arctgX(long double x, long double e);

#endif // ARCTGX_H
