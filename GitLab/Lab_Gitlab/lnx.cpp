/*! \file
\brief Файл исходного кода функции для вычисления натурального логарифма от x
*/
#include "lnx.h"
/*!
\param[in] x Аргумент
\param[in] e Точность
\return Значение натурального логарифма от x с точностью e
*/
long double lnX(long double x, long double e)
{
    long double curUpper = x;
    long long int curBottom = 1;
    int sign = -1;
    long double add = 0;
    long double res = x;
    do
    {
        curUpper = curUpper * x;
        curBottom++;
        add = curUpper / curBottom * sign;
        if ((add>=e) || (add<=-e))
        {
            res += add;
        }
        sign = -sign;
    } while ((add>=e) || (add<=-e));
    return res;
}
