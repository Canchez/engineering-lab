/*! \file
\brief Заголовочный файл функции для вычисления (1+x) в степени a
*/
#ifndef BRACKETINA_H
#define BRACKETINA_H
/*!
\param[in] x Аргумент
\param[in] e Точность
\param[in] a Степень скобки
\return Значение (1+x) в степени a с точностью e
*/
long double bracketInA(long double x, long double e, long double a);

#endif // BRACKETINA_H
